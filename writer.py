from google.cloud import bigquery
import os

credentials_path = '/pytkey.json'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credentials_path

client = bigquery.Client()
table_id = 'rising-amphora-332105.HomeApp.Device_Data'

rows_to_insert = [
    {u'ID':'101', u'deviceID':'5522',u'CO':'45',u'TVOC':'32',u'Temp':'35'},
]

errors = client.insert_rows_json(table_id, rows_to_insert)
if errors == []:
    print('New rows have been added.')
else:
    print(f'Encountered errors while inserting rows: {errors}')
